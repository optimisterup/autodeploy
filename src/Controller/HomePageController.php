<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Alexey Nazarov <alexey.nazarov@sibers.com>
 */
class HomePageController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @return Response
     */
    public function number(): Response
    {
        return $this->render('homePage/homepage.html.twig');
    }
}

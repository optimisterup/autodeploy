<?php

namespace App\Command;

use Exception;
use Symfony\Component\Process\Process;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;

/**
 * @author Alexey Nazarov <alexey.nazarov@sibers.com>
 */
class CreateDataBaseBackupCommand extends Command
{
    const DB_SCHEME = 'mysql';

    /**
     * @var string
     */
    protected static $defaultName = 'app:db:backup';

    /**
     * @var string
     */
    public $scheme;

    /**
     * @var string
     */
    public $dbName;

    /**
     * @var string
     */
    public $user;

    /**
     * @var string
     */
    public $pass;

    /**
     * @var string
     */
    public $port;

    /**
     * @var string
     */
    public $projectDir;

    /**
     * @param string $scheme
     * @param string $user
     * @param string $pass
     * @param string $dbName
     * @param string $port
     */
    public function __construct(string $scheme, string $user, string $pass, string $dbName, string $port)
    {
        parent::__construct();

        $this->scheme = $scheme;
        $this->user   = $user;
        $this->pass   = $pass;
        $this->dbName = $dbName;
        $this->port   = $port;
    }

    /**
     * Configurator
     */
    protected function configure()
    {
        $this->setDescription('Create DB backup')
        ->addArgument('backupFolderAndName', InputArgument::REQUIRED, 'Full path and name to save backup (for example: pojectDIR/shared/backup/db_backup_name.sql)');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws Exception DateTime Exception.
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Starting create db backup');

        if (self::DB_SCHEME === $this->scheme) {
            $process = Process::fromShellCommandline("mysqldump -u $this->user -p$this->pass $this->dbName > ".$input->getArgument('backupFolderAndName'));
            $process->run();

            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            } else {
                $io->success('DB backup has been creating successfully.');
            }
        } else {
            $io->error('Support only mysql db scheme !');
        }
    }
}

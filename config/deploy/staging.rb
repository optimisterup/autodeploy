# The server-based syntax can be used to override options:
# ------------------------------------
server 'gw2.sibers.com',
  user: 'nazarov',
  roles: %w{web app},
  port: '1157',
  ssh_options: {
    forward_agent: false,
    auth_methods: %w(publickey password),
    keys: %w(/home/nazarov/.ssh/dev7),
  }
  set :deploy_to, '/var/www/html/personal/nazarov/autodeploy'
  set :branch, "master"
  set :file_permissions_groups, ["apache"]
  set :composer_install_flags, '--no-dev --no-interaction --optimize-autoloader'
  set :application, "autodeploy"
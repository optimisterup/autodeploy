# The server-based syntax can be used to override options:
# ------------------------------------
server 'gw2.sibers.com',
  user: 'nazarov',
  roles: %w{web app dev},
  port: '1157',
  ssh_options: {
    forward_agent: false,
    auth_methods: %w(publickey password),
    keys: %w(/home/nazarov/.ssh/id_rsa),
  }
  set :deploy_to, '/var/www/html/personal/nazarov/autodeploy'
  set :branch, "development"
  set :file_permissions_groups, ["phpteam"]
  set :application, "nazarov.personal.dev7.sibers.com"
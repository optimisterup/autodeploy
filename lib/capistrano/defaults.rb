# This checks that the version of Capistrano running the configuration is the same as was intended to run it.
lock "~> 3.11.1"
set :stage, fetch(:stage)
set :repo_url, "git@bitbucket.org:optimisterup/autodeploy.git"
set :deploy_via, :remote_cache

# Default value for keep_releases is 5
set :keep_releases, 3

# DB backup configs
set :db_backup_name, "db_backup_name.sql"
set :backup_dir,     "#{shared_path}/backup"

#Symfony
set :symfony_directory_structure, 3
set :sensio_distribution_version, 5

# symfony-standard edition directories
set :app_path,    ""
set :web_path,    "public"
set :var_path,    "var"
set :bin_path,    "bin"

# The next settings are lazily evaluated from the above values, so take care when modifying them
set :app_config_path, "config"
set :log_path,        "var/log"
set :cache_path,      "var/cache"
set :session_path,    "var/sessions"

# symfony console
set :symfony_console_path,           "bin/console"
set :symfony_console_flags,          "--no-debug"

# Remove app_dev.php during deployment, other files in web/ can be specified here
set :controllers_to_clear, []

# asset management
set :assets_install_path,    "#{fetch :web_path}"
set :assets_install_flags,   "--symlink"
set :composer_install_flags, "--no-interaction --optimize-autoloader"

# Share files/directories between releases
set :linked_dirs, ["#{fetch :log_path}", "#{fetch :session_path}"]
set :linked_files, %w{}

# Set correct permissions between releases, this is turned off by default
set :permission_method, :acl
set :file_permissions_paths, ["#{fetch :var_path}"]

after   "deploy:check:directories",       "deploy:check_write_permissions"
after   "deploy:check:linked_dirs",       "deploy:check_dirs_and_files_permissions"
before  "composer:run",                   "deploy:rewrite_params"
before  "deploy:updated",                 "deploy:migrate"
after   "deploy:finishing",               "deploy:set_permissions:acl"
after   "deploy:set_permissions:acl",     "deploy:check_cache_permissions"
after   "deploy:cleanup",                 "deploy:create_symlink_to_web"
after   "deploy:create_symlink_to_web",   "db:backup"

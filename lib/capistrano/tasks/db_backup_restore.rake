namespace :db do
    task :backup do
        backup_dir = "#{shared_path}/backup"
        on roles(:db) do
        info "--> Checking backup directory... #{fetch :backup_dir}"
            if !test ("[ -d #{backup_dir} ]")
                info "--> Creating backup path..."
                execute "mkdir -p #{fetch :backup_dir}"
            end
            info "--> Backup directory is exist ... #{fetch :backup_dir}"
            within "#{release_path}" do
                info "--> Creating backup for DB ..."
                symfony_console('app:db:backup', "#{backup_dir}/#{fetch :db_backup_name}")
                info "--> Creating backup for composer.json ..."
                execute(:cp, "composer.json","#{backup_dir}/composer.json")
            end
        end
    end
    task :restore do
        backup_dir = "#{shared_path}/backup"
        on roles(:db) do
            within "#{release_path}" do
                info "--> Dropping database with error..."
                symfony_console('doctrine:schema:drop', '--force')

                info "--> Restoring database from last successfully backup..."
                symfony_console('doctrine:database:import', "#{backup_dir}/#{fetch :db_backup_name}")

                info "--> Restoring composer.json from last successfully backup..."
                execute(:cp, "#{backup_dir}/composer.json", "composer.json")
            end
        end
    end
end
namespace :deploy do
    desc "Check that we can writable access"
    task :check_write_permissions do
        on roles(:web) do |host|
            if test("[ -w #{fetch(:deploy_to)} ]")
                info "#{fetch(:deploy_to)} is writable on #{host}"
            else
                text = "#{fetch(:deploy_to)} is not writable on #{host}"
                error "#{text}"
                raise "#{text}"
            end
        end
    end
    desc "Check permissions for folder and files"
    task :check_dirs_and_files_permissions do
        on roles(:web) do
           def find_directories_and_files (path, type)
                begin
                    searchType = ("d" === type) ? "directories" : "files"
                    result = capture(:find, "#{path} -type #{type} -not -executable -not -writable -prune |& grep -v 'Permission denied'")
                rescue
                    info "~> All #{searchType} have write permissions"
                end
                if result
                    text = "Have not permissions for #{searchType} \n #{result}"
                    error "#{text}"
                    raise "#{text}"
                end
           end
           find_directories_and_files("#{current_path}/{./#{fetch :cache_path},./public}", "d")
           find_directories_and_files("#{current_path}/#{fetch :cache_path}", "f")
        end
    end
    task :rewrite_params do
        stage = fetch(:stage)
        on roles(:web) do
            within "#{release_path}" do
                execute(:cp, ".env.#{stage}",".env.local")
            end
        end
    end
    task :check_cache_permissions do
        cache_path = fetch(:cache)
        on roles(:web) do
          execute "chmod -R 0777 #{release_path}/#{cache_path}"
        end
    end
    task :create_symlink_to_web do
        on roles(:web) do
            execute(:ln, "-sfn", "#{current_path}/#{fetch :web_path}", "#{deploy_to}/#{fetch :web_path}")
        end
    end
    task :migrate do
        on roles(:db) do
            begin
                symfony_console('doctrine:migrations:migrate', '--no-interaction')
            rescue

                error "Migrations with errors!"
                invoke "db:restore"
                invoke "deploy:rm_failed"
                text = "~> Please, fix migrations, and try again later"
                error "#{text}"
                raise "#{text}"
            end
        end
    end
    desc 'Delete failed release'
    task :rm_failed do
        on roles(:web) do
            info "--> Deleting failed release..."
            if test "[ -d #{current_path} ]"
                    current_release = capture(:readlink, "-f #{current_path}").to_s
                    path_release    = capture(:readlink, "-f #{release_path}").to_s

                    info "--> Path release ~> #{path_release}"
                    if current_release != path_release
                            execute :rm, "-rf #{path_release}"
                            info "--> Release has been successfully deleted..."
                        else
                            error "--> Release has not been deleted..."
                            error  "--> Current release ~> #{current_release}"
                    end
                else
                    text = "--> Current directory ~> #{current_release} dos not exist"
                    error "#{text}"
                    raise "#{text}"
            end
        end
    end
end